/*
Navicat MySQL Data Transfer

Source Server         : media
Source Server Version : 50623
Source Host           : 563aa99e32727.sh.cdb.myqcloud.com:4202
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2016-10-13 11:04:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for event_custom
-- ----------------------------
DROP TABLE IF EXISTS `event_custom`;
CREATE TABLE `event_custom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `kfAccount` varchar(255) DEFAULT NULL,
  `toKfAccount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for event_follow
-- ----------------------------
DROP TABLE IF EXISTS `event_follow`;
CREATE TABLE `event_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `type` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for event_location
-- ----------------------------
DROP TABLE IF EXISTS `event_location`;
CREATE TABLE `event_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(50) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `eventKey` varchar(60) DEFAULT NULL,
  `type` varchar(60) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `precision` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1544 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for event_mass
-- ----------------------------
DROP TABLE IF EXISTS `event_mass`;
CREATE TABLE `event_mass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  `totalCount` varchar(20) DEFAULT NULL,
  `filterCount` varchar(20) DEFAULT NULL,
  `sentCount` varchar(20) DEFAULT NULL,
  `errorCount` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for event_menu
-- ----------------------------
DROP TABLE IF EXISTS `event_menu`;
CREATE TABLE `event_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `eventKey` varchar(500) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1297 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for event_qr_code
-- ----------------------------
DROP TABLE IF EXISTS `event_qr_code`;
CREATE TABLE `event_qr_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `eventKey` varchar(60) DEFAULT NULL,
  `ticket` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for event_shake
-- ----------------------------
DROP TABLE IF EXISTS `event_shake`;
CREATE TABLE `event_shake` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `uuid` varchar(60) DEFAULT NULL,
  `major` varchar(60) DEFAULT NULL,
  `minor` varchar(60) DEFAULT NULL,
  `distance` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='摇一摇';

-- ----------------------------
-- Table structure for event_speech
-- ----------------------------
DROP TABLE IF EXISTS `event_speech`;
CREATE TABLE `event_speech` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `mediaId` varchar(100) DEFAULT NULL,
  `format` varchar(60) DEFAULT NULL,
  `recognition` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for event_template
-- ----------------------------
DROP TABLE IF EXISTS `event_template`;
CREATE TABLE `event_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_image
-- ----------------------------
DROP TABLE IF EXISTS `msg_image`;
CREATE TABLE `msg_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `picUrl` varchar(255) DEFAULT NULL,
  `mediaId` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_image_miao
-- ----------------------------
DROP TABLE IF EXISTS `msg_image_miao`;
CREATE TABLE `msg_image_miao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `picUrl` varchar(255) DEFAULT NULL,
  `mediaId` varchar(100) DEFAULT NULL,
  `picId` varchar(60) DEFAULT NULL,
  `picCode` varchar(60) DEFAULT NULL,
  `resultPicUrl` varchar(255) DEFAULT NULL,
  `resultPicCode` varchar(60) DEFAULT NULL,
  `result` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_link
-- ----------------------------
DROP TABLE IF EXISTS `msg_link`;
CREATE TABLE `msg_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `url` text,
  `status` varchar(20) DEFAULT NULL,
  `readAmount` int(11) DEFAULT NULL,
  `likeAmount` int(11) DEFAULT NULL,
  `sort` varchar(20) DEFAULT '0',
  `groupId` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_location
-- ----------------------------
DROP TABLE IF EXISTS `msg_location`;
CREATE TABLE `msg_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `label` varchar(20) DEFAULT NULL,
  `location_Y` varchar(20) DEFAULT NULL,
  `location_X` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `event` varchar(60) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `precision` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_short_video
-- ----------------------------
DROP TABLE IF EXISTS `msg_short_video`;
CREATE TABLE `msg_short_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `mediaId` varchar(100) DEFAULT NULL,
  `thumbMediaId` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_text
-- ----------------------------
DROP TABLE IF EXISTS `msg_text`;
CREATE TABLE `msg_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `fromUserNickName` varchar(255) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_text_reply
-- ----------------------------
DROP TABLE IF EXISTS `msg_text_reply`;
CREATE TABLE `msg_text_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `fromUserNickName` varchar(255) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_video
-- ----------------------------
DROP TABLE IF EXISTS `msg_video`;
CREATE TABLE `msg_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `mediaId` varchar(100) DEFAULT NULL,
  `thumbMediaId` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg_voice
-- ----------------------------
DROP TABLE IF EXISTS `msg_voice`;
CREATE TABLE `msg_voice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msgId` varchar(60) DEFAULT NULL,
  `fromUserName` varchar(100) DEFAULT NULL,
  `toUserName` varchar(100) DEFAULT NULL,
  `createTime` varchar(20) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `mediaId` varchar(100) DEFAULT NULL,
  `format` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
