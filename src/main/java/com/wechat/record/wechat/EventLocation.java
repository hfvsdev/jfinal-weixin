/**
 * @Description:
 * @author dimmer
 * @date 2015年10月24日 上午9:33:33
 *
 */
package com.wechat.record.wechat;

import java.util.Date;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 *
 * @ProjectName：wechatplat
 * @ClassName：EventIn @Description：
 * @author: dimmer
 * @date: 2015年10月24日 上午9:33:33
 *
 */
public class EventLocation extends Model<EventLocation> {
	public static EventLocation me = new EventLocation();

	public EventLocation queryByOpenId(String openId) {
		List<EventLocation> locations = me
				.find("select * from event_location where fromUserName='" + openId + "' order by createTime desc ");
		if (null == locations || 0 == locations.size()) {
			return null;
		}
		return locations.get(0);
	}


	public EventLocation saveTmpLocation(String openId, String address, String latitude, String longitude) {
		EventLocation location = new EventLocation();
		location.set("fromUserName", openId);
		location.set("createTime", new Date());
		location.set("type", "SELF");
		location.set("latitude", latitude);
		location.set("longitude", longitude);
		location.set("msgType", address);
		location.save();
		return location;
	}
}
