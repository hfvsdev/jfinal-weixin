/**
 * @Description:
 * @author dimmer
 * @date 2015年10月24日 上午9:33:33
 *
 */
package com.wechat.record.wechat;

import java.io.InputStream;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

/**
 *
 * @ProjectName：wechatplat
 * @ClassName：EventIn @Description：
 * @author: dimmer
 * @date: 2015年10月24日 上午9:33:33
 *
 */
public class MsgLink extends Model<MsgLink> {
	public static MsgLink me = new MsgLink();

	/**
	 * 查询
	 *
	 * @param entity
	 * @return
	 */
	public List<MsgLink> query(MsgLink entity) {
		String[] keys = entity._getAttrNames();
		StringBuffer buffer = new StringBuffer("select * from msg_link where 1=1 ");
		for (String key : keys) {
			buffer.append(" and ").append(key).append("='").append(entity.get(key) + "").append("' ");
		}
		buffer.append(" order by createTime desc ");
		return me.find(buffer.toString());
	}

	public List<Record> queryLink(MsgLink entity) {
		String[] keys = entity._getAttrNames();
		StringBuffer buffer = new StringBuffer(
				"select link.*,cus.nickName,cus.headImgUrl from msg_link link LEFT JOIN cif_customer  cus on link.fromUserName = cus.extendId where 1=1 ");
		for (String key : keys) {
			buffer.append(" and link.").append(key).append("='").append(entity.get(key) + "").append("' ");
		}
		buffer.append(" order by createTime desc ");
		return Db.find(buffer.toString());
	}


}
