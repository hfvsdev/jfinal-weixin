/**
 * @Description:
 * @author dimmer
 * @date 2015年10月24日 上午9:33:33
 *
 */
package com.wechat.record.wechat;

import java.util.Date;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

/**
 *
 * @ProjectName：wechatplat
 * @ClassName：EventIn @Description：
 * @author: dimmer
 * @date: 2015年10月24日 上午9:33:33
 *
 */
public class MsgText extends Model<MsgText> {
	public static MsgText me = new MsgText();

	/**
	 * 查询近48小时内的聊天请求
	 *
	 * @return
	 */
	public static List<Record> queryChats() {
		return Db.find(
				"select count(1) as cou,fromUserNickName,fromUserName,content,createTime from msg_text where msgType='text' and createTime>UNIX_TIMESTAMP(TIMESTAMPADD(DAY,-2,CURRENT_TIMESTAMP)) GROUP BY fromUserName ");
	}

	/**
	 * 查询聊天记录:用type区分类型,reply是客服回复的消息
	 *
	 * @param openId
	 * @return
	 */
	public static List<MsgText> query(String openId) {
		return me.find("select * from msg_text where fromUserName='" + openId + "'");
	}

	/**
	 * 回复客户
	 *
	 * @param openId
	 * @param operatorId
	 * @param content
	 */
	public static void reply(String openId, String operatorId, String content) {
		// 记录发送消息
		MsgText text = new MsgText();
		text.set("content", content);
		text.set("fromUserName", openId);
		text.set("toUserName", operatorId);
		text.set("createTime",new Date().getTime());
		text.set("msgType", "reply");
		text.save();
		// 给客户发送消息
	}
}
