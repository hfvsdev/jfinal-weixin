/**
 * @Description:
 * @author dimmer
 * @date 2015年10月24日 上午9:33:33
 *
 */
package com.wechat.record.wechat;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @ProjectName：wechatplat
 * @ClassName：EventIn @Description：
 * @author: dimmer
 * @date: 2015年10月24日 上午9:33:33
 * 
 */
public class MsgLocation extends Model<MsgLocation> {
	public static MsgLocation me = new MsgLocation();

	public MsgLocation queryByOpenId(String openId) {
		List<MsgLocation> locations = me.find("select * from msg_location where fromUserName='" + openId + "' order by createTime desc ");
		if (null == locations || 0 == locations.size()) {
			return null;
		}
		return locations.get(0);
	}
}
