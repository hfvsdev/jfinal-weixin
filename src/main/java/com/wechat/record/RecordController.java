/**
 * @Description:
 * @author dimmer
 * @date 2015年10月24日 上午9:34:05
 *
 */
package com.wechat.record;

import com.jfinal.core.Controller;
import com.jfinal.weixin.sdk.msg.in.InImageMsg;
import com.jfinal.weixin.sdk.msg.in.InLinkMsg;
import com.jfinal.weixin.sdk.msg.in.InLocationMsg;
import com.jfinal.weixin.sdk.msg.in.InShortVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.InVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InVoiceMsg;
import com.jfinal.weixin.sdk.msg.in.event.InCustomEvent;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InLocationEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMassEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InShakearoundUserShakeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InTemplateMsgEvent;
import com.jfinal.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;
import com.wechat.record.wechat.EventCustom;
import com.wechat.record.wechat.EventFollow;
import com.wechat.record.wechat.EventLocation;
import com.wechat.record.wechat.EventMass;
import com.wechat.record.wechat.EventMenu;
import com.wechat.record.wechat.EventQrCode;
import com.wechat.record.wechat.EventShake;
import com.wechat.record.wechat.EventSpeech;
import com.wechat.record.wechat.EventTemplate;
import com.wechat.record.wechat.MsgImage;
import com.wechat.record.wechat.MsgLink;
import com.wechat.record.wechat.MsgLocation;
import com.wechat.record.wechat.MsgShortVideo;
import com.wechat.record.wechat.MsgText;
import com.wechat.record.wechat.MsgVideo;
import com.wechat.record.wechat.MsgVoice;

/**
 *
 * @ProjectName：wechatplat
 * @ClassName：RecordController @Description：
 * @author: dimmer
 * @date: 2015年10月24日 上午9:34:05
 *
 */
public class RecordController extends Controller {
	public static void recordEventCustom(InCustomEvent record) {
		EventCustom entity = new EventCustom();
		entity.set("createTime", record.getCreateTime());
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordEventFollow(InFollowEvent record) {
		EventFollow entity = new EventFollow();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordEventLocation(InLocationEvent record) {
		EventLocation entity = new EventLocation();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordEventMass(InMassEvent record) {
		EventMass entity = new EventMass();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordEventMenu(InMenuEvent record) {
		EventMenu entity = new EventMenu();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordEventQrCode(InQrCodeEvent record) {
		EventQrCode entity = new EventQrCode();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordEventShake(InShakearoundUserShakeEvent record) {
		EventShake entity = new EventShake();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordEventSpeech(InSpeechRecognitionResults record) {
		EventSpeech entity = new EventSpeech();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordEventTemplate(InTemplateMsgEvent record) {
		EventTemplate entity = new EventTemplate();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordMsgLocation(InLocationMsg record) {
		MsgLocation entity = new MsgLocation();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordMsgText(InTextMsg record) {
		MsgText entity = new MsgText();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordMsgImage(InImageMsg record) {
		MsgImage entity = new MsgImage();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordMsgLink(InLinkMsg record) {
		MsgLink entity = new MsgLink();
		entity.set("status","STAT0101");
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 组装链接
		entity.set("url",entity.getStr("url"));

		entity.save();
	}

	public static void recordMsgShortVideo(InShortVideoMsg record) {
		MsgShortVideo entity = new MsgShortVideo();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordMsgVideo(InVideoMsg record) {
		MsgVideo entity = new MsgVideo();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}

	public static void recordMsgVoice(InVoiceMsg record) {
		MsgVoice entity = new MsgVoice();
		try {
			entity._setAttrs(BeanUtil.bean2Map(record));
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.save();
	}
}
